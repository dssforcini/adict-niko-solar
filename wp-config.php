<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'adict_nikosolar' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'W:x<ye5T21JA@,4JVAfXl!w;}mEvEyM>$#|idj-;WcYRPLuEu}MO--<1q_Ot}#ex' );
define( 'SECURE_AUTH_KEY',  'fB-LcM$>8cc(JV]:=n19bjrx*?G2v+9MJupAVe2CLqE9vG|ITP#:t49BhHHO 5vJ' );
define( 'LOGGED_IN_KEY',    'm%iph;SGahKxI ^Ma^wY8`,OKhuS|`mZWd03^t/[qPPN;SH2Xp>r3y-_q!ASs/G(' );
define( 'NONCE_KEY',        '8O,A)_S {y#+4@_g?Ynd<^fB2U9T(G0gB!niuLvD@sM_eqiycs>=J_u3h/}4!GX~' );
define( 'AUTH_SALT',        'iTj1qX;*{#m)Melw3tVH$,8%|K!lhNZh<v]s:i$MzI ;*,yy.~H0I5H8RXn6:2RE' );
define( 'SECURE_AUTH_SALT', '@oj=.Z$hnD$*^TY4!iZ+)F}>>h$W%qmAhy#M@t?<[w/[{;NR+))} kxjLfE*C*6=' );
define( 'LOGGED_IN_SALT',   '? nide30v{!&iu03e`}aKR_JL]y13n{?M#/WpSEdq(kC9~6*e:h+LL-rAYD q qM' );
define( 'NONCE_SALT',       '!7;r7cb{4qD<dvJZ^rV}(hUq#/>_B.%AU6H8??1pR[/TkNjxF)j:*ZLhll,s}aW#' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_adict_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
