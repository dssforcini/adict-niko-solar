<?php get_header(); ?>
<div class="container-fluid cf-pagina">
	<div class="container c-manuais">
		<div class="row">
			<div class="col-lg-12 col-titulo">
				<h2 class="cor-azul">MANUAIS DISPONÍVEIS</h2>
				<div class="woocommerce">
					<?php woocommerce_breadcrumb(); ?>
				</div>
			</div>
			<?php if (have_posts()) { ?>
				<?php while (have_posts()) { the_post(); ?>
					<div class="col-lg-12 bloco-manual">
						<div class="bloco-manual-interno">
							<a href="<?php the_field('opcao_arquivo_download'); ?>" target="_blank" alt="Baixar <?php the_title(); ?>" title="Baixar <?php the_title(); ?>">
								<img src="<?php echo TEMA_URL.'/img/icone-download.svg'; ?>" alt="Baixar <?php the_title(); ?>" title="Baixar <?php the_title(); ?>">
								<span><?php the_title(); ?></span>
							</a>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>