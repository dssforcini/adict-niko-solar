<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-faq">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-titulos text-center">
				<h2>FAQ</h2>
				<h3>Perguntas e Respostas</h3>
			</div>
			<div class="col-lg-12 col-descricao text-center">
				<p>Encontre aqui as respostas para as dúvidas mais comuns de nossos clientes, caso não encontrar sua pergunta, por favor, entre em contato conosco clicando <a href="<?php echo home_url('fale-conosco'); ?>">aqui</a>.</p>
			</div>
			<div class="col-lg-12 col-perguntas-respostas">
				<?php
					$faq = new WP_Query(
						array(
							'post_type' => 'cpt_faq',
							'order' => 'DESC',
							'posts_per_page' => -1
						)
					);
				?>
				<?php /* ?>
				<?php if ($faq->have_posts()) { ?>
					<?php while ($faq->have_posts()) { $faq->the_post(); ?>
						<?php $idResposta = 'resposta_'.get_the_ID(); ?>
						<div class="bloco-pergunta-resposta">
							<div class="pergunta">
								<button type="button" class="btn btn-primary" data-toggle="collapse" href="#<?php echo $idResposta; ?>" role="button" aria-expanded="false" aria-controls="<?php echo $idResposta; ?>"><?php the_title(); ?></button>
							</div>
							<div class="resposta collapse" id="<?php echo $idResposta; ?>">
								<?php the_content(); ?>
							</div>
						</div>
					<?php } wp_reset_postdata(); ?>
				<?php } ?>
				<?php */ ?>

				<?php if ($faq->have_posts()) { ?>
					<div class="accordion" id="accordionFaq">
						<?php while ($faq->have_posts()) { $faq->the_post(); ?>
							<?php $idResposta = 'collapse_'.get_the_ID(); ?>
							<?php $idHeading = 'heading_'.get_the_ID(); ?>
							<div class="card bloco-pergunta-resposta">
								<div class="card-header pergunta" id="<?php echo $idHeading; ?>">
									<button class="btn botao" type="button" data-toggle="collapse" data-target="#<?php echo $idResposta; ?>" aria-expanded="false" aria-controls="<?php echo $idResposta; ?>"><?php the_title(); ?></button>
								</div>
								<div id="<?php echo $idResposta; ?>" class="collapse resposta" aria-labelledby="<?php echo $idHeading; ?>" data-parent="#accordionFaq">
									<div class="card-body"><?php the_content(); ?></div>
								</div>
							</div>
						<?php } wp_reset_postdata(); ?>
					</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>