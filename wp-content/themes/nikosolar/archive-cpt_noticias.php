<?php get_header(); ?>
<div class="container-fluid cf-pagina">
	<div class="container c-pagina c-noticias-archive">
		<div class="row">
			<div class="col-lg-12">
				<?php if (is_tax()) { ?>
					<?php $queried = get_queried_object(); ?>
					<h2 class="titulo-inicio-pagina">NOTÍCIAS > <?php echo $queried->name; ?></h2>
				<?php } else { ?>
					<h2 class="titulo-inicio-pagina">NOTÍCIAS</h2>
				<?php } ?>
			</div>
			<div class="col-lg-12">
				<?php // echo do_shortcode('[breadcrumb]'); ?>
				<div class="woocommerce">
					<?php woocommerce_breadcrumb(); ?>
				</div>
			</div>
			<div class="col-lg-8 col-lista-noticias">
				<?php if (have_posts()) { ?>
					<?php while (have_posts()) { the_post(); ?>
						<div class="bloco-noticia">
							<div class="titulo">
								<a href="<?php the_permalink(); ?>">
									<h4 class="texto"><?php the_title(); ?></h4>
								</a>
							</div>
							<div class="categorias">
								<?php
									$categorias = get_the_terms(get_the_ID(), 'tax_noticias_categoria');
									// echo '<pre>';
									// echo print_r($categorias);
									// echo '</pre>';
									foreach ($categorias as $index => $categoria) {
										$categoria_nome = $categoria->name;
										$categoria_link = get_term_link($categoria->term_id, 'tax_noticias_categoria');
										$categoria_separador = ((($index+1) < count($categorias)) ? ' | ' : '');
										echo '<a href="'.$categoria_link.'">'.$categoria_nome.'</a>'.$categoria_separador;
									}
								?>
							</div>
							<div class="imagem">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
								</a>
							</div>
							<div class="resumo">
								<p><?php echo strip_tags(get_the_excerpt()); ?></p>
							</div>
							<div class="text-right continuar-lendo">
								<a href="<?php the_permalink(); ?>">CONTINUAR LENDO</a>
							</div>
						</div>
					<?php } wp_reset_postdata(); ?>
					<div class="paginacao">
						<?php pagination_bar(); ?>
					</div>
				<?php } ?>
			</div>
			<div class="col-lg-4 col-barra-lateral-noticias">
				<?php echo do_shortcode('[sidebar-noticias]'); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>