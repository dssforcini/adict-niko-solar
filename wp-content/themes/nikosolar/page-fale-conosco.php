<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-fale-conosco">
	<div class="container c-pagina c-fale-conosco">
		<div class="row r-pagina">
			<div class="col-lg-12">
				<h2 class="titulo-inicio-pagina"><?php the_title(); ?></h2>
			</div>
			<div class="col-lg-12">
				<?php // echo do_shortcode('[breadcrumb]'); ?>
				<div class="woocommerce">
					<?php woocommerce_breadcrumb(); ?>
				</div>
			</div>
			<div class="col-lg-12 estilo-texto descricao-inicio-pagina">
				<?php the_field('opcao_informacoes_descricao', 'options'); ?>
			</div>
			<div class="col-lg-12 nopadding formularios">
				<div class="container-fluid cf-formularios">
					<div class="row r-formularios">
						<?php
							$formularios = get_field('opcao_formularios', 'options');
							// echo '<pre>';
							// echo print_r($formularios);
							// echo '</pre>';
						?>
						<div class="col-lg-3 coluna-titulo-formulario">
							<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
								<?php foreach ($formularios as $index => $formulario) { ?>
									<a class="nav-link <?php echo (($index == 0) ? 'active' : ''); ?> tab-formulario" id="v-pills-<?php echo $index ?>-tab" data-toggle="pill" href="#v-pills-<?php echo $index ?>" role="tab" aria-controls="v-pills-<?php echo $index ?>" aria-selected="true">
										<span><?php echo $formulario['opcao_formulario_nome']; ?></span>
									</a>
								<?php } ?>
							</div>
						</div>
						<div class="col-lg-9 coluna-conteudo-formulario">
							<div class="h-100 tab-content" id="v-pills-tabContent">
								<?php foreach ($formularios as $index => $formulario) { ?>
									<div class="h-100 tab-pane <?php echo (($index == 0) ? 'show active' : ''); ?> tab-pane-formulario" id="v-pills-<?php echo $index ?>" role="tabpanel" aria-labelledby="v-pills-<?php echo $index ?>-tab">
										<div class="descricao-formulario estilo-texto">
											<?php echo $formulario['opcao_formulario_descricao']; ?>
										</div>
										<div class="formulario-container">
											<div class="container-fluid">
												<?php echo do_shortcode($formulario['opcao_formulario_shortcode']); ?>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12 coluna-titulo-secundario-pagina">
				<h4 class="titulo-secundario-pagina"><?php the_field('opcao_titulo_informacoes_contato', 'options'); ?></h4>
			</div>
			<div class="col-lg-12 separador-secundario-pagina">
				<span></span>
			</div>
			<div class="col-lg-12 estilo-texto">
				<?php the_field('opcao_informacoes_contato', 'options'); ?>
			</div>
			<div class="col-lg-12 coluna-dados-contato">
				<div class="dados-contato dados-contato-endereco">
					<div class="dados-contato-interno dados-contato-endereco-interno">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/svg/map-pointer.svg'; ?>" alt="Endereço">
						</div>
						<div class="conteudo">
							<?php the_field('opcao_endereco', 'options'); ?>
						</div>
					</div>
				</div>
				<?php if (have_rows('opcao_telefones', 'options')) { ?>
					<?php while (have_rows('opcao_telefones', 'options')) { the_row(); ?>
						<div class="dados-contato dados-contato-telefones">
							<div class="dados-contato-interno dados-contato-telefones-interno">
								<div class="icone">
									<?php if (get_sub_field('opcao_telefone_whatsapp', 'options')) { ?>
										<img src="<?php echo TEMA_URL.'/svg/whatsapp.svg'; ?>" alt="WhatsApp">
									<?php } else { ?>
										<img src="<?php echo TEMA_URL.'/svg/telefone.svg'; ?>" alt="Telefone">
									<?php } ?>
								</div>
								<div class="conteudo">
									<span><?php the_sub_field('opcao_telefone', 'options'); ?></span>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				<div class="dados-contato dados-contato-email">
					<div class="dados-contato-interno dados-contato-email-interno">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/svg/envelope.svg'; ?>" alt="E-mail">
						</div>
						<div class="conteudo">
							<span><?php the_field('opcao_email', 'options'); ?></span>
						</div>
					</div>
				</div>
				<div class="dados-contato dados-contato-skype">
					<div class="dados-contato-interno dados-contato-skype-interno">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/svg/skype.svg'; ?>" alt="Skype">
						</div>
						<div class="conteudo">
							<span><?php the_field('opcao_skype', 'options'); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$.ajax({
			url: '<?php echo TEMA_URL.'/ajax-busca-opcoes-sac.php'; ?>',
			type: 'POST',
			success: function(data) {
				$('#opcao_sac').append(data);
				// console.log(data);
			}
		});
		$.ajax({
			url: '<?php echo TEMA_URL.'/ajax-busca-area-atuacao.php'; ?>',
			type: 'POST',
			success: function(data) {
				$('#comboAreaAtuacao').append(data);
				// console.log(data);
			}
		});
		$('#consumomensalrange').val($('#consumomensalrange').attr('min'));
		$('span.numero_total_kwh').html($('#consumomensalrange').attr('min')+'kWh');
		$(document).on('input', '#consumomensalrange', function(event) {
			$('#txtConsumoMedioMensalText').val($(this).val());
			$('span.numero_total_kwh').html($(this).val()+'kWh');
		});

	});
</script>
<?php get_footer(); ?>