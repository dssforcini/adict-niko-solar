<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	
	<div class="bloco-inicial-produto">
		<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		// do_action( 'woocommerce_before_single_product_summary' );

		$imagens_ids = $product->get_gallery_attachment_ids();
		$imagens = array();
		$imagens[] = get_the_post_thumbnail_url();
		foreach ($imagens_ids as $imagem_id) {
			$imagens[] = wp_get_attachment_url($imagem_id);
		}

		echo '<div class="images galeria-imagens-produto">';
			echo '<div class="galeria-maior">';
				foreach ($imagens as $imagem) {
					echo '<div class="bloco-imagem-maior">';
						echo '<div class="h-100 bloco-imagem-maior-interno">';
							echo '<a data-fancybox="galeria_'.get_the_ID().'" href="'.$imagem.'">';
								echo '<img src="'.$imagem.'" class="h-100 w-100" alt="'.get_the_title().'">';
							echo '</a>';
						echo'</div>';
					echo'</div>';
				}
			echo'</div>';
			echo '<div class="galeria-menor">';
				foreach ($imagens as $imagem) {
					echo '<div class="bloco-imagem-menor">';
						echo '<div class="h-100 bloco-imagem-menor-interno">';
							echo '<img src="'.$imagem.'" class="h-100 w-100" alt="'.get_the_title().'">';
						echo'</div>';
					echo'</div>';
				}
			echo'</div>';
		echo'</div>';

		?>

		<script>
			jQuery(document).ready(function($) {
				$('.galeria-maior').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					fade: true,
					asNavFor: '.galeria-menor'
				});
				$('.galeria-menor').slick({
					slidesToShow: 5,
					slidesToScroll: 1,
					vertical: true,
					verticalSwiping: true,
					asNavFor: '.galeria-maior',
					dots: false,
					arrows: false,
					centerMode: false,
					focusOnSelect: true
				});
			});
		</script>

		<div class="summary entry-summary">
			<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );
			?>
		</div>
	</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
