<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="profile" href="https://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		
		<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
		
		<?php wp_head(); ?>

		<?php // === CSS === // ?>
		<link rel="stylesheet" type="text/css" href="<?php echo TEMA_URL.'/css/dss-layout.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo TEMA_URL.'/css/bootstrap.min.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo TEMA_URL.'/css/slick.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo TEMA_URL.'/css/jquery-ui.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo TEMA_URL.'/css/jquery.fancybox.min.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo TEMA_URL.'/style.css' ?>">

		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

		<?php // === JS === // ?>
		<script src="<?php echo TEMA_URL.'/js/jquery-3.4.1.min.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/dss-layout.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/bootstrap.min.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/jquery-ui.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/jquery.fancybox.min.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/slick.min.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/jquery.mask.min.js'; ?>"></script>
		<script src="<?php echo TEMA_URL.'/js/same-height.js'; ?>"></script>

		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

		<script>
			jQuery(document).ready(function($) {
				var SPMaskBehavior = function (val) {
				  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
				},
				spOptions = {
				  onKeyPress: function(val, e, field, options) {
				      field.mask(SPMaskBehavior.apply({}, arguments), options);
				    }
				};

				$('.telcel').mask(SPMaskBehavior, spOptions);
			});
		</script>
	</head>
	<body <?php body_class(); ?>>

		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v4.0"></script>

		<!-- <div id="loader_page"> -->
			<!-- <img src="<?php echo TEMA_URL.'/svg/loader_pagina.svg'; ?>" alt=""> -->
		<!-- </div> -->
		<header>
			<div class="container-fluid cf-cabecalho">
				<div class="container c-cabecalho">
					<div class="row r-cabecalho">
						<div class="col-lg-4 d-flex align-items-center col-cabecalho col-cabecalho-logo">
							<div class="col-cabecalho-logo-interno">
								<a href="<?php echo home_url(); ?>">
									<img src="<?php echo home_url('wp-content/uploads/2019/07/logo.png'); ?>" alt="<?php bloginfo('name'); echo ' | '; bloginfo('description'); ?>">
								</a>
							</div>
						</div>
						<div class="col-lg-4 d-flex align-items-center col-cabecalho col-cabecalho-busca">
							<div class="col-cabecalho-busca-interno">
								<?php get_product_search_form(); ?>
							</div>
						</div>
						<div class="col-lg-2 d-flex align-items-center col-cabecalho col-cabecalho-login">
							<div class="col-cabecalho-interno col-cabecalho-login-interno">
								<div class="icone">
									<img src="<?php echo TEMA_URL.'/svg/area-cliente.svg' ?>" alt="Entrar ou Cadastrar">
								</div>
								<div class="conteudo">
									<?php if (is_user_logged_in()) { ?>
										<p class="titulo">ÁREA DO CLIENTE</p>
										<p><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Minha Conta</a></p>
									<?php } else { ?>
										<p class="titulo">ÁREA DO CLIENTE</p>
										<p><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Entre</a> ou <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">cadastre-se</a></p>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="col-lg-2 d-flex align-items-center col-cabecalho col-cabecalho-carrinho">
							<div class="col-cabecalho-interno col-cabecalho-carrinho-interno">
								<div class="icone">
									<img src="<?php echo TEMA_URL.'/svg/carrinho.svg' ?>" alt="Carrinho">
								</div>
								<div class="conteudo">
									<p class="titulo">MEU CARRINHO</p>
									<p><a href="<?php echo wc_get_cart_url(); ?>"><span class="qtdItensCarrinho"><?php echo WC()->cart->get_cart_contents_count(); ?>&nbsp;<?php echo ((WC()->cart->get_cart_contents_count() == 1) ? 'produto' : 'produtos'); ?></span></a></p>
								</div>
							</div>
							<?php produtos_carrinho(); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid cf-menu">
				<div class="container c-menu">
					<div class="row r-menu">
						<div class="col-lg-12 d-none col-botao-menu">
							<button class="btn btn-info w-100 btn-collapse" type="button" data-toggle="collapse" data-target="#collapseMenu" aria-expanded="true" aria-controls="collapseMenu">
							<span class="texto">MENU</span>
							<span class="icone"></span>
							</button>
						</div>
						<div class="col-lg-10 collapse col-menu col-menu-items" id="collapseMenu">
							<?php require_once('menu.php'); ?>
							<ul class="menu">
								<?php foreach ($menus as $menu) { ?>

									<?php if ($menu['opcao_menu_visivel']) { ?>

										<li class="item-menu <?php echo basename($menu['opcao_menu_url']); ?> <?php echo ((is_page(basename($menu['opcao_menu_url']))) ? 'ativo' : ''); ?>">
											<?php // if ($menu['opcao_menu_url'] == 'produtos') { ?>
											<?php if (strpos($menu['opcao_menu_url'], 'produtos') !== false) { ?>
												<a href="javascript:void(0);" class="item-menu-categorias collapsed" data-toggle="collapse" data-target="#collapseCategorias" aria-expanded="false" aria-controls="collapseCategorias">
													<span><?php echo $menu['opcao_menu_titulo']; ?></span>
												</a>
												<?php /* ?>
												<?php
													$categorias = get_terms(
														array(
															'taxonomy' => 'product_cat',
															'hide_empty' => true,
															'parent' => 0,
															'exclude' => array(179, 15)
														)
													);
												?>
												<ul class="categorias collapse" id="collapseCategorias">
													<?php foreach ($categorias as $categoria) { ?>
														<?php if ($categoria->slug != 'uncategorized') { ?>
															<li class="categoria-item">
																<a href="<?php echo get_term_link($categoria->term_id); ?>"><?php echo $categoria->name; ?></a>
																<?php $categorias_filhas = get_term_children($categoria->term_id, 'product_cat'); ?>
																<?php if (is_array($categorias_filhas)) { ?>
																	<ul class="subcategorias">
																		<?php foreach ($categorias_filhas as $categoria_filha) { ?>
																			<?php $cat = get_term_by('id', $categoria_filha, 'product_cat'); ?>
																			<?php if ($cat->count >= 1) { ?>
																				<li class="subcategoria-item">
																					<a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
																				</li>
																			<?php } ?>
																		<?php } ?>
																	</ul>
																<?php } ?>
															</li>
														<?php } ?>
													<?php } ?>
												</ul>
												<?php */ ?>
												<?php
													$categoria_escolhida = explode('-', $menu['opcao_menu_url']);
													$categoria_escolhida = ((count($categoria_escolhida) > 1) ? $categoria_escolhida[1] : 0);
												?>
												<?php get_tax_hierarchical('product_cat', $categoria_escolhida, array('15', '179')); ?>
											<?php } else { ?>
												<a href="<?php echo $menu['opcao_menu_url']; ?>" target="<?php echo (($menu['opcao_menu_nova_guia'] == true) ? '_blank' : '_self'); ?>">
													<span><?php echo $menu['opcao_menu_titulo']; ?></span>
												</a>
											<?php } ?>
										</li>
										
									<?php } ?>

								<?php } ?>
							</ul>
						</div>
						<div class="col-lg-2 col-menu col-redes-sociais">
							<div class="rede-social rede-social-facebook">
								<div class="rede-social-interno rede-social-facebook-interno">
									<a href="<?php the_field('opcao_link_facebook', 'options'); ?>" target="_blank">
										<img src="<?php echo TEMA_URL.'/svg/facebook.svg'; ?>" class="" alt="Facebook">
									</a>
								</div>
							</div>
							<div class="rede-social rede-social-instagram">
								<div class="rede-social-interno rede-social-instagram-interno">
									<a href="<?php the_field('opcao_link_instagram', 'options'); ?>" target="_blank">
										<img src="<?php echo TEMA_URL.'/svg/instagram.svg'; ?>" class="" alt="Instagram">
									</a>
								</div>
							</div>
							<div class="rede-social rede-social-linkedin">
								<div class="rede-social-interno rede-social-linkedin-interno">
									<a href="<?php the_field('opcao_link_linkedin', 'options'); ?>" target="_blank">
										<img src="<?php echo TEMA_URL.'/svg/linkedin.svg'; ?>" class="" alt="Linkedin">
									</a>
								</div>
							</div>
							<div class="rede-social rede-social-youtube">
								<div class="rede-social-interno rede-social-youtube-interno">
									<a href="<?php the_field('opcao_link_youtube', 'options'); ?>" target="_blank">
										<img src="<?php echo TEMA_URL.'/svg/youtube.svg'; ?>" class="" alt="YouTube">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<script>
			jQuery(document).ready(function($) {
				var tela_width = $(window).width();
				console.log(tela_width);
				if (tela_width <= 980) {
					$('.col-botao-menu').removeClass('d-none');
					$('.col-menu-items').removeClass('show');
				} else {
					$('.col-botao-menu').addClass('d-none');
					$('.col-menu-items').addClass('show');
					
					$('ul.categorias').removeAttr('id');
					$('ul.categorias').addClass('show');
					
				}

				/*
				$(document).ready(function () {
				    $(document).click(function (event) {
				        var clickover = $(event.target);
				        var _opened = $(".categorias.collapse").hasClass("categorias collapse show");
				        if (_opened === true && !clickover.attr("collapse")) {
				            $(".item-menu-categorias").click();
				        }
				    });
				});
				*/
				/*
				$(document).ready(function () {
				    $(document).click(function (event) {
				        var clickover = $(event.target);
				        var _opened = $(".categorias.collapse").hasClass("categorias collapse show");
				        if (_opened === true && !clickover.attr("collapse")) {
				            $(".categorias.collapse").collapse('hide');
				        }
				    });
				});
				*/
			});
		</script>
		<?php  ?>
		<div class="pagina">