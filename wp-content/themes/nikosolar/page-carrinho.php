<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-carrinho">
	<div class="container c-pagina c-carrinho">
		<div class="row r-pagina">
			<div class="col-lg-12">
				<?php echo do_shortcode('[woocommerce_cart]'); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>