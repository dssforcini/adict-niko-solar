<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-checkout">
	<div class="container c-pagina c-checkout">
		<div class="row r-pagina">
			<div class="col-lg-12">
				<?php echo do_shortcode('[woocommerce_checkout]'); ?>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('input, textarea, select').each(function(index, el) {
			if ($(this).attr('type') != 'checkbox') {
				if (!$(this).hasClass('form-control')) {
					$(this).addClass('form-control');
				}
			}	
		});
	});
</script>
<?php get_footer(); ?>