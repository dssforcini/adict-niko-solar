<?php get_header(); ?>
<?php the_post(); ?>
<div class="container-fluid cf-pagina">
	<div class="container c-pagina c-interna-noticia">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="titulo-inicio-pagina"><?php the_title(); ?></h2>
				<div class="woocommerce">
					<?php woocommerce_breadcrumb(); ?>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="text-center imagem-destacada">
					<?php the_post_thumbnail('full', array('class' => 'img-fluid', 'title' => get_the_title(), 'alt' => get_the_title())); ?>
				</div>
				<div class="text-justify conteudo">
					<?php the_content(); ?>
				</div>
				<div class="compartilhar">
					<p>Compartilhe:</p>
					<?php echo do_shortcode('[addtoany]'); ?>
				</div>
				<div class="comentarios">
					<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="10"></div>
				</div>
			</div>
			<div class="col-lg-4">
				<?php echo do_shortcode('[sidebar-noticias]'); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>