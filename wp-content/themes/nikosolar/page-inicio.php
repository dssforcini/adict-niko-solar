<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-inicio">
	<div class="row r-slides">
		<div class="col-lg-12 nopadding coluna-slides">
			<div class="slider" data-aos="fade-up">
				<?php if (have_rows('opcao_slides', 'options')) { ?>
					<?php while (have_rows('opcao_slides', 'options')) { the_row(); ?>
						<div class="slide">
							<div class="slide-interno">
								<a href="<?php the_sub_field('opcao_slide_link', 'options'); ?>" target="<?php echo ((get_sub_field('opcao_slide_link_nova_guia')) ? '_blank' : '_seld'); ?>">
									<img src="<?php the_sub_field('opcao_slide_imagem', 'options'); ?>" class="imagem-desktop" alt="<?php the_sub_field('opcao_slide_titulo', 'options'); ?>" title="<?php the_sub_field('opcao_slide_titulo', 'options'); ?>">
									<img src="<?php the_sub_field('opcao_slide_imagem_mobile', 'options'); ?>" class="imagem-mobile" alt="<?php the_sub_field('opcao_slide_titulo', 'options'); ?>" title="<?php the_sub_field('opcao_slide_titulo', 'options'); ?>">
								</a>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
		<div class="container c-destaques-produtos">
			<div class="row r-destaques-produtos">
				<div class="col-lg-12 col-titulo-destaques-produtos">
					<h2 class="titulo">DESTAQUES</h2>
				</div>
				<div class="col-lg-12 col-destaques-produtos">
					<?php echo do_shortcode('[featured_products per_page="12" columns="4"]'); ?>
				</div>
				<div class="col-lg-12 text-center col-todos-produtos">
					<a href="<?php echo home_url('loja'); ?>">VER TODOS OS PRODUTOS</a>
					<span></span>
				</div>
			</div>
		</div>
		<div class="container c-blocos-finais">
			<div class="row r-blocos-finais">
				<?php if (have_rows('opcao_blocos_finais', 'options')) { ?>
					<?php while (have_rows('opcao_blocos_finais', 'options')) { the_row(); ?>
						<div class="col-lg-4 bloco-final">
							<div class="bloco-final-interno">
								<a href="<?php the_sub_field('opcao_blocos_finais_link'); ?>">
									<img src="<?php the_sub_field('opcao_blocos_finais_imagem'); ?>" alt="<?php the_sub_field('opcao_blocos_finais_titulo'); ?>">
									<div class="descricao">
										<span><?php the_sub_field('opcao_blocos_finais_titulo'); ?></span>
									</div>
								</a>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
	<script>
		jQuery(document).ready(function($) {
			$('.slider').slick({
				dots: true,
				infinite: true
			});
		});
	</script>
</div>
<?php get_footer(); ?>