<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-minha-conta">
	<div class="container c-pagina c-minha-conta">
		<div class="row r-pagina">
			<div class="col-lg-12">
				<?php echo do_shortcode('[woocommerce_my_account]'); ?>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('input, textarea, select').each(function(index, el) {
			if ($(this).attr('type') != 'checkbox') {
				if (!$(this).hasClass('form-control')) {
					$(this).addClass('form-control');
				}
			}	
		});
	});
</script>
<?php get_footer(); ?>