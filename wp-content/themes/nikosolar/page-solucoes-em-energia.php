<?php get_header(); ?>
<div class="container-fluid cf-pagina cf-solucoes-energia">
	<div class="row r-itens-fundo" style=""></div>
	<div class="row r-itens">
		<div class="col-lg-12 text-center">
			<img src="<?php echo home_url('wp-content/uploads/2019/07/icone.png'); ?>" alt="Ícone Niko Solar">
		</div>
		<div class="col-lg-12">
			<h1 class="cor-preto titulo-bola">NIKO SOLAR <span class="cor-azul">ENERGIA LIMPA E LIVRE</span></h1>
		</div>
		<div class="col-lg-12 descricao estilo-texto">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam quaerat praesentium eos cum ut cupiditate necessitatibus laudantium tempore consequatur delectus! Eligendi ipsum dolor, numquam sapiente recusandae quidem totam odio doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias tempora laboriosam ab accusamus nesciunt odit, omnis vero placeat nostrum sunt, assumenda ratione ut id aperiam doloribus quidem fugiat aliquid deserunt!
		</div>
		<div class="col-lg-4 item-pagina text-center" referencia="">
			<button type="button" class="btn btn-default" alvo="item-pagina-residencial">Residencial</button>
		</div>
		<div class="col-lg-4 item-pagina text-center">
			<button type="button" class="btn btn-default" alvo="item-pagina-comercial">Comercial</button>
		</div>
		<div class="col-lg-4 item-pagina text-center">
			<button type="button" class="btn btn-default" alvo="item-pagina-parceiros">Parceiros</button>
		</div>
		<div class="col-lg-6 item-pagina text-center">
			<button type="button" class="btn btn-default" alvo="item-pagina-outras-aplicacoes">Outras aplicações</button>
		</div>
		<div class="col-lg-6 item-pagina text-center">
			<button type="button" class="btn btn-default" alvo="item-pagina-qual-custo">Qual é o custo?</button>
		</div>
		<span class="botao-scroll-down"></span>
	</div>
	<div class="row r-residencial-comercial">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 solucao-pagina solucao-pagina-residencial" id="item-pagina-residencial">
					<div class="topico-inicio">
						<h3 class="cor-preto-titulo">PARA <span class="cor-azul">SUA CASA</span></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
					</div>
					<div class="topico topico-residencial">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/grafico-linha.png'; ?>" alt="Valorize Seu Imóvel">
						</div>
						<div class="descricao">
							<p>Valorize seu imóvel</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
						</div>
					</div>
					<div class="topico topico-residencial">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/ideia.png'; ?>" alt="Fim das altas tarifas">
						</div>
						<div class="descricao">
							<p>Fim das altas tarifas</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
						</div>
					</div>
					<div class="topico topico-residencial">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/ferramenta.png'; ?>" alt="Fácil instalação">
						</div>
						<div class="descricao">
							<p>Fácil instalação</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2 text-center coluna-central">
					<div class="icone">
						<img src="<?php echo TEMA_URL.'/img/solucoes/painel-solar.png'; ?>" alt="Painel Solar">
					</div>
				</div>
				<div class="col-lg-5 solucao-pagina solucao-pagina-comercial" id="item-pagina-comercial">
					<div class="topico-inicio">
						<h3 class="cor-preto-titulo">PARA <span class="cor-azul">SUA EMPRESA</span></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
					</div>
					<div class="topico topico-comercial">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/barras.png'; ?>" alt="Reduza custos">
						</div>
						<div class="descricao">
							<p>Reduza custos</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
						</div>
					</div>
					<div class="topico topico-comercial">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/valor.png'; ?>" alt="Agregue valor">
						</div>
						<div class="descricao">
							<p>Agregue valor</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
						</div>
					</div>
					<div class="topico topico-comercial">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/capacete.png'; ?>" alt="Instalação">
						</div>
						<div class="descricao">
							<p>Instalação</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam explicabo, exercitationem nam quos consectetur.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row r-quadro-amarelo">
		<div class="col-lg-3 bloco">
			<div class="bloco-interno">
				<div class="icone">
					<img src="<?php echo TEMA_URL.'/img/solucoes/quadro-amarelo/dia.png'; ?>" alt="Durante o Dia">
				</div>
				<div class="conteudo">
					<h3>Durante o Dia</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, tempore aliquid blanditiis amet.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 bloco">
			<div class="bloco-interno">
				<div class="icone">
					<img src="<?php echo TEMA_URL.'/img/solucoes/quadro-amarelo/noite.png'; ?>" alt="Durante a Noite">
				</div>
				<div class="conteudo">
					<h3>Durante a Noite</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, tempore aliquid blanditiis amet.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 bloco">
			<div class="bloco-interno">
				<div class="icone">
					<img src="<?php echo TEMA_URL.'/img/solucoes/quadro-amarelo/nublado.png'; ?>" alt="Quando Nublado">
				</div>
				<div class="conteudo">
					<h3>Quando Nublado</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, tempore aliquid blanditiis amet.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 bloco">
			<div class="bloco-interno">
				<div class="icone">
					<img src="<?php echo TEMA_URL.'/img/solucoes/quadro-amarelo/com-energia.png'; ?>" alt="Sobrando Energia">
				</div>
				<div class="conteudo">
					<h3>Sobrando Energia</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, tempore aliquid blanditiis amet.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 bloco">
			<div class="bloco-interno">
				<div class="icone">
					<img src="<?php echo TEMA_URL.'/img/solucoes/quadro-amarelo/sem-energia.png'; ?>" alt="Faltando Energia">
				</div>
				<div class="conteudo">
					<h3>Faltando Energia</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, tempore aliquid blanditiis amet.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row r-custo-aplicacao">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="titulo">
						<h2 class="cor-preto">QUAL É O CUSTO DA <span class="cor-azul">ENERGIA SOLAR?</span></h2>
					</div>
					<div class="subtitulo">
						<h4 class="cor-preto texto">Economize na conta e valorize seu imóvel.</h4>
					</div>
					<div class="conteudo cor-preto">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut odio cupiditate eveniet vitae perferendis veritatis ex accusamus laborum eos fugit facere quod ea, corrupti quos dicta autem commodi explicabo omnis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam itaque aliquam laborum pariatur ipsam accusantium? Reiciendis incidunt sit nesciunt autem at quaerat quam illo expedita ut aliquam. Aspernatur, iste, molestiae.
					</div>
				</div>
				<div class="col-lg-4">
					<div class="botao-calcule">
						<div class="texto">
							<a href="#">
								<p>CALCULE O</p>
								<p>INVESTIMENTO</p>
								<p>NECESSÁRIO</p>
							</a>
						</div>
						<div class="imagem">
							<a href="#">
								<img src="<?php echo TEMA_URL.'/img/solucoes/calculadora.png'; ?>" alt="Calculadora Solar">
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 coluna-itens-custo">
					<div class="bloco-custo">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/contas.png'; ?>" alt="Reduza as contas">
						</div>
						<div class="conteudo">
							<p class="titulo">Reduza as contas</p>
							<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veniam vero nam, ad recusandae libero corporis delectus.</p>
						</div>
					</div>
					<div class="bloco-custo">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/sustentabilidade.png'; ?>" alt="Sustentabilidade">
						</div>
						<div class="conteudo">
							<p class="titulo">Sustentabilidade</p>
							<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veniam vero nam, ad recusandae libero corporis delectus.</p>
						</div>
					</div>
					<div class="bloco-custo">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/barras.png'; ?>" alt="Monitoramento">
						</div>
						<div class="conteudo">
							<p class="titulo">Monitoramento</p>
							<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veniam vero nam, ad recusandae libero corporis delectus.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 coluna-itens-custo">
					<div class="bloco-custo">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/ideia.png'; ?>" alt="Créditos">
						</div>
						<div class="conteudo">
							<p class="titulo">Créditos</p>
							<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veniam vero nam, ad recusandae libero corporis delectus.</p>
						</div>
					</div>
					<div class="bloco-custo">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/grafico-linha.png'; ?>" alt="Valorização do imóvel">
						</div>
						<div class="conteudo">
							<p class="titulo">Valorização do imóvel</p>
							<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veniam vero nam, ad recusandae libero corporis delectus.</p>
						</div>
					</div>
					<div class="bloco-custo">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/img/solucoes/suporte.png'; ?>" alt="Suporte">
						</div>
						<div class="conteudo">
							<p class="titulo">Suporte</p>
							<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veniam vero nam, ad recusandae libero corporis delectus.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row d-flex align-items-center justify-content-center r-depoimentos">
		<div class="col-lg-6 d-flex align-items-center justify-content-center text-center col-depoimentos">
			<h2 class="titulo-depoimentos">DEPOIMENTOS</h2>
			<?php /* ?>
			<div class="depoimentos">
				<?php if (have_rows('opcao_depoimentos', 'options')) { ?>
					<?php while (have_rows('opcao_depoimentos', 'options')) { the_row(); ?>
						<div class="depoimento">
							<div class="descricao">
								<?php the_sub_field('opcao_dep_descricao'); ?>
							</div>
							<div class="autor">
								<p><?php the_sub_field('opcao_dep_autor'); ?></p>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php */ ?>
			<div class="depoimentos">
				<?php
					$depoimentos = new WP_Query(
						array(
							'post_type' => 'cpt_depoimentos',
							'posts_per_page' => -1
						)
					);
				?>
				<?php if ($depoimentos->have_posts()) { ?>
					<?php while ($depoimentos->have_posts()) { $depoimentos->the_post(); ?>
						<div class="depoimento">
							<div class="descricao">
								<?php the_content(); ?>
							</div>
							<div class="autor">
								<p><?php the_title(); ?></p>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
		<script>
			jQuery(document).ready(function($) {
				$('.depoimentos').slick({
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplay: true,
					autoplaySpeed: 2000,
				});
			});
		</script>
	</div>
	<div class="row r-outras-aplicacoes">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 bloco-inicio">
					<div class="titulo">
						<h2 class="cor-preto">OUTRAS <span class="cor-azul">APLICAÇÕES</span></h2>
					</div>
					<div class="subtitulo">
						<h4 class="cor-preto texto">Sistemas isolados da rede | off-grid</h4>
					</div>
					<div class="descricao">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget scelerisque leo. Aenean ut vulputate leo. Nulla ut pharetra nibh, in bibendum risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam lacinia sit amet arcu nec porttitor. Nunc eget pellentesque urna. Suspendisse consequat, mauris et faucibus mollis, justo lorem lobortis urna, ac imperdiet justo est a est.
					</div>
				</div>
				<div class="col-lg-6 bloco-aplicacao">
					<div class="bloco-aplicacao-interno">
						<div class="imagem">
							<img src="<?php echo home_url('wp-content/uploads/2019/08/casas-isoladas.png'); ?>" alt="Casas isoladas">
						</div>
						<div class="descricao">
							<div class="titulo">Casas Isoladas</div>
							<div class="texto">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 bloco-aplicacao">
					<div class="bloco-aplicacao-interno">
						<div class="imagem">
							<img src="<?php echo home_url('wp-content/uploads/2019/08/postes-solares-iluminacao.png'); ?>" alt="Postes solares e iluminação">
						</div>
						<div class="descricao">
							<div class="titulo">Postes solares e iluminação</div>
							<div class="texto">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 bloco-aplicacao">
					<div class="bloco-aplicacao-interno">
						<div class="imagem">
							<img src="<?php echo home_url('wp-content/uploads/2019/08/rodovias.png'); ?>" alt="Rodovias">
						</div>
						<div class="descricao">
							<div class="titulo">Rodovias</div>
							<div class="texto">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 bloco-aplicacao">
					<div class="bloco-aplicacao-interno">
						<div class="imagem">
							<img src="<?php echo home_url('wp-content/uploads/2019/08/telecomunicacoes.png'); ?>" alt="Telecomunicações">
						</div>
						<div class="descricao">
							<div class="titulo">Telecomunicações</div>
							<div class="texto">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 bloco-aplicacao">
					<div class="bloco-aplicacao-interno">
						<div class="imagem">
							<img src="<?php echo home_url('wp-content/uploads/2019/08/bombas-agua.png'); ?>" alt="Bombas de água">
						</div>
						<div class="descricao">
							<div class="titulo">Bombas de água</div>
							<div class="texto">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 bloco-aplicacao">
					<div class="bloco-aplicacao-interno">
						<div class="imagem">
							<img src="<?php echo home_url('wp-content/uploads/2019/08/eletrificacao-rural.png'); ?>" alt="Eletrificação rural">
						</div>
						<div class="descricao">
							<div class="titulo">Eletrificação rural</div>
							<div class="texto">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row r-parceiro">
		<div class="container">
			<div class="row"> 	
				<div class="col-lg-6 coluna-texto">
					<div class="titulo">
						<h2 class="cor-preto">PARCEIRO <span class="cor-azul">NIKOSOLAR</span></h2>
					</div>
					<div class="subtitulo">
						<h4 class="cor-preto texto">Por quê trabalhar conosco?</h4>
					</div>
					<div class="descricao">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget scelerisque leo. Aenean ut vulputate leo. Nulla ut pharetra nibh, in bibendum risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam lacinia sit amet arcu nec porttitor. Nunc eget pellentesque urna. Suspendisse consequat, mauris et faucibus mollis, justo lorem lobortis urna, ac imperdiet justo est a est.
					</div>
					<div class="vantagens">
						<p class="titulo-vantagens">Vantagens em caminhar com a Niko</p>
						<div class="vantagem">
							<p class="titulo-vantagem">Arquitetos e Engenheiros</p>
							<p class="descricao-vantagem">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
						<div class="vantagem">
							<p class="titulo-vantagem">Construtores</p>
							<p class="descricao-vantagem">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
						<div class="vantagem">
							<p class="titulo-vantagem">Instaladores</p>
							<p class="descricao-vantagem">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
					<div class="botao-parceiro">
						<a href="#">COMECE A TRABALHAR CONOSCO AGORA MESMO</a>
					</div>
				</div>
				<div class="col-lg-6 coluna-borda"></div>
			</div>
		</div>
	</div>
	<div class="row r-faq">
		<div class="container">
			<div class="col-lg-12 text-center">
				<div class="titulo">
					<h2 class="cor-branco">TIRE TODAS SUAS DÚVIDAS</h2>
				</div>
				<div class="descricao">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
				</div>
				<div class="botoes">
					<a href="<?php echo home_url('faq'); ?>" class="botao-consulte">CONSULTE NOSSA FAQ</a>
					<a href="<?php echo home_url('fale-conosco'); ?>" class="botao-contato">ENTRE EM CONTATO</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>