		</div>
		<footer>
			<div class="container-fluid cf-acesso-rapido">
				<div class="container c-acesso-rapido">
					<div class="row r-botao-acesso-rapido">
						<div class="col-lg-12 aba-acesso-rapido">
							<p class="collapsed" data-toggle="collapse" href="#collapseAcessoRapido" role="button" aria-expanded="false" aria-controls="collapseAcessoRapido">CATEGORIAS<span class="texto-acesso-rapido">(acesso rápido)</span></p>
						</div>
					</div>
					<div class="row collapse r-acesso-rapido" id="collapseAcessoRapido">
						<?php
							$categorias = get_terms(
								array(
									'taxonomy' => 'product_cat',
									'hide_empty' => true,
									'parent' => 0,
									'exclude' => array(179, 15)
								)
							);

							foreach ($categorias as $categoria) {
								if ($categoria->slug != 'uncategorized') {
									echo '<div class="col-sm-12 col-md-6 col-lg-2 categorias">';
										echo '<div class="titulo-categoria">';
											echo '<h5 class="texto"><a href="'.get_term_link($categoria->term_id, 'product_cat').'">'.$categoria->name.'</a></h5>';
										echo'</div>';
										$filhas = get_term_children($categoria->term_id, 'product_cat');
										if (is_array($filhas)) {
											echo '<div class="categorias-filhas">';
												echo '<ul>';
													foreach ($filhas as $filha) {
														$categoria_filha = get_term_by('id', $filha, 'product_cat');
														if ($categoria_filha->count >= 1) {
															echo '<li>';
																echo '<a href="'.get_term_link($categoria_filha->term_id, 'product_cat').'">'.$categoria_filha->name.'</a>';
															echo'</li>';	
														}
													}
												echo'</ul>';
											echo'</div>';
										}
									echo'</div>';
								}
							}
						?>
					</div>
				</div>
				<script>
					jQuery(document).ready(function($) {
						$('.aba-acesso-rapido p').on('click', function(event) {
							$('html').animate({
								scrollTop: $(".r-botao-acesso-rapido").offset().top
							}, 500);

						});
					});
				</script>
			</div>
			<div class="container-fluid cf-widgets">
				<div class="container c-widgets">
					<div class="row r-widgets">
						<div class="col-lg-3 col-rodape col-rodape-contato">
							<div class="col-rodape-interno col-rodape-contato-interno">
								<div class="atendimento">
									<div class="titulo">
										<h3 class="texto">ATENDIMENTO AO CLIENTE</h3>
									</div>
									<div class="conteudo">
										<?php
											$contador_telefone = 1;
											$telefones = get_field('opcao_telefones', 'options');
											// echo '<pre>';
											// echo print_r($telefones);
											// echo '</pre>';
											foreach ($telefones as $telefone) {
												echo (($contador_telefone == 1) ? '<div>' : '');
												echo '<div class="d-inline-block telefone"><div class="telefone-interno"><span>'.$telefone['opcao_telefone'].'</span>'.(($telefone['opcao_telefone_whatsapp'] == true) ? '<img src="'.TEMA_URL.'/svg/whatsapp.svg'.'" alt="WhatsApp">' : '').'</div></div>';
												echo (($contador_telefone == 2) ? '</div>' : '');
												$contador_telefone = (($contador_telefone == 2) ? 1 : ($contador_telefone + 1));
											}
										?>
										<div class="email">
											<?php $email = get_field('opcao_email', 'options'); ?>
											<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
										</div>
									</div>
								</div>
								<div class="endereco">
									<div class="titulo">
										<h3 class="texto">ENDEREÇO</h3>
									</div>
									<div class="conteudo">
										<?php the_field('opcao_endereco', 'options'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-rodape col-rodape-link-uteis">
							<div class="col-rodape-interno col-rodape-link-uteis-interno">
								<div class="titulo">
									<h3 class="texto">LINKS ÚTEIS</h3>
								</div>
								<div class="conteudo">
									<?php $links = get_field('opcao_links_uteis', 'options'); ?>
									<ul class="lista-links">
										<?php foreach ($links as $link) { ?>
											<li class="item-link">
												<a href="<?php echo $link['opcao_link_util_url_titulo'] ?>" target="<?php echo (($link['opcao_link_util_nova_guia'] == true) ? '_blank' : '_self'); ?>"><?php echo $link['opcao_link_util_titulo'] ?></a>
											</li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-rodape col-rodape-solucoes">
							<div class="col-rodape-interno col-rodape-solucoes-interno">
								<div class="titulo">
									<h3 class="texto">SOLUÇÕES</h3>
								</div>
								<div class="conteudo">
									<?php $links = get_field('opcao_solucoes', 'options'); ?>
									<ul class="lista-solucoes">
										<?php foreach ($links as $link) { ?>
											<li class="item-solucao">
												<a href="<?php echo $link['opcao_link_util_url_titulo'] ?>" target="<?php echo (($link['opcao_link_util_nova_guia'] == true) ? '_blank' : '_self'); ?>"><?php echo $link['opcao_link_util_titulo'] ?></a>
											</li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-rodape col-rodape-pagamento">
							<div class="col-rodape-interno col-rodape-pagamento-interno">
								<div class="titulo">
									<h3 class="texto">FORMAS DE PAGAMENTO</h3>
								</div>
								<div class="conteudo">
									<img src="<?php the_field('opcao_formas_pagamento', 'options'); ?>" class="img-fluid" alt="Formas de Pagamento">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid cf-desenvolvido">
				<div class="container c-desenvolvido">
					<div class="row r-desenvolvido">
						<div class="col-lg-6 d-flex align-items-center col-desenvolvido col-desenvolvido-niko">
							<p><span class="bold">Niko Solar</span> | Todos os direitos reservados.</p>
						</div>
						<div class="col-lg-6 d-flex align-items-center col-desenvolvido col-desenvolvido-adict">
							<p><span class="bold">DESENVOLVIDO POR:</span> <a href="https://adict.com.br" target="_blank"><img src="<?php echo home_url('wp-content/uploads/2019/07/logo-adict.png'); ?>" alt="Adict Comunicação"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<script>
		  AOS.init();
		</script>
	</body>
	<?php wp_footer(); ?>
</html>