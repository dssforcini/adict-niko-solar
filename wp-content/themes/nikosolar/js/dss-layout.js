jQuery(document).ready(function($) {

	/* ================================ */
	/* ============ INPUTS ============ */
	/* ================================ */
	atualizaInputs();

	$('body').on('focus', '.campo-input .input', function(event) {
		$(this).parent('.campo-input').addClass('focus');
	});

	$('body').on('blur', '.campo-input .input', function(event) {
		if ($(this).val().length >= 1) {
			$(this).parent('.campo-input').addClass('tem-conteudo');
			if (!$(this).parent('.campo-input').children('.limpar-input').length) {
				$(this).parent('.campo-input').append('<span class="limpar-input"></span>');
			}
		} else {
			$(this).parent('.campo-input').removeClass('tem-conteudo');
			if ($(this).parent('.campo-input').children('.limpar-input').length) {
				$(this).parent('.campo-input').children('.limpar-input').remove();
			}
		}
		$(this).parent('.campo-input').removeClass('focus');
	});

	$('body').on('click', '.limpar-input', function(event) {
		$(this).parent('.campo-input').removeClass('tem-conteudo');
		$(this).parent('.campo-input').children('.input').val('');
		$(this).remove();
	});

	$('body').on('click', 'input.botao-select', function(event) {
		$(this).parent('.campo-input').children('.opcoes-select').addClass('ativo');
	});

	$('body').on('click', function(event) {
		if (!$(event.target).hasClass('botao-select')) {
			$('.opcoes-select').removeClass('ativo');
		}
	});

	$('body').on('click', '.opcao-select', function(event) {
		console.log($(this).attr('valor'));
		$(this).parent().parent().parent().parent('.campo-input').children('select.input').val($(this).attr('valor'));
		$(this).parent().parent().parent().parent('.campo-input').addClass('tem-conteudo');
		$(this).parent().parent().parent().parent('.campo-input').children('input.botao-select').val($(this).html());
	});

	function atualizaInputs() {
		$('.campo-input .input').each(function(index, el) {
			if ($(this).val().length >= 1) {
				$(this).parent('.campo-input').addClass('tem-conteudo');
				$(this).parent('.campo-input').append('<span class="limpar-input"></span>');
			} else {
				$(this).parent('.campo-input').children('.limpar-input').remove();
			}

			if ($(this).is(':disabled')) {
				$(this).parent('.campo-input').toggleClass('desabilitado');
			}

			/* === textarea === */
			if ($(this).is('textarea')) {
				$(this).parent('.campo-input').toggleClass('textarea');
			}

			/* === select === */
			if ($(this).is('select')) {

				$(this).parent('.campo-input').removeClass('tem-conteudo');
				$(this).parent('.campo-input').children('.limpar-input').remove();
				$(this).parent('.campo-input').toggleClass('select');

				if ($(this).is(':disabled')) {
					$(this).parent('.campo-input').append('<input class="botao-select" readonly="true" disabled="disabled">');
				} else {
					$(this).parent('.campo-input').append('<input class="botao-select" readonly="true">');
					let opcoes = '<div class="opcoes-select">';
					opcoes += '<ul class="lista">';
					let tem_selecionado = false;
					let item_selecionado = '';

					$('option', this).each(function(index, el) {
						console.log('VALOR: ' + $(this).val() + ' | HTML: ' + $(this).html());
						
						let selected = '';
						if ($(this).is(':selected')) {
							selected = 'selecionado';
							tem_selecionado = true;
							item_selecionado = $(this).html();
						}
						
						opcoes += '<li><a href="javascript:void(0);" valor="'+$(this).val()+'" class="opcao-select" selecionado="'+selected+'">'+$(this).html()+'</a></li>';
					});
					opcoes += '</ul>';
					opcoes += '</div>';

					if (tem_selecionado) {
						$(this).parent('.campo-input').addClass('tem-conteudo');
						$(this).parent('.campo-input').children('input.botao-select').val(item_selecionado);
					}

					$(this).parent('.campo-input').append(opcoes);
				}

			}
		});
	}
});