<?php

define('TEMA_URL', get_stylesheet_directory_uri());

add_filter( 'widget_text', 'do_shortcode' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Opções Gerais',
		'menu_title'	=> 'Opções do Site',
		'menu_slug' 	=> 'opcoes-site',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Página Início',
		'menu_title'	=> 'Página Início',
		'parent_slug'	=> 'opcoes-site',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Página Fale Conosco',
		'menu_title'	=> 'Página Fale Conosco',
		'parent_slug'	=> 'opcoes-site',
	));
	/*
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Página Soluções em Energia Solar',
		'menu_title'	=> 'Página Soluções em Energia Solar',
		'parent_slug'	=> 'opcoes-site',
	));
	*/
}

function iconic_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );


function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// limitando a 2 noticias por página no archive noticias
function wp_noticias_query_archive( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'cpt_noticias' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', 2 );
    }
}
add_action( 'pre_get_posts', 'wp_noticias_query_archive' );


function pagination_bar( $query = null) {
    global $wp_query;    $query = $query ? $query : $wp_query;
 
    $total_pages = $query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
        
        $big = 999999999;
        echo '<div class="custom-paginacao">'.paginate_links(array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%', 
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text' => __( '&laquo;' ),
            'next_text' => __( '&raquo;' ),
        )).'</div>';
    }

    ?>
        <style type="text/css">
            .custom-paginacao {
                display: flex;
                align-items: center;
                justify-content: center;
                padding: 5px;
            }
            .custom-paginacao a,
            .custom-paginacao span {
                padding: 6px 12px;
                border: 1px solid #ddd;
                margin-left: -1px;
                text-decoration: none;
                color: #333333;
                font-size: 20px;
                font-family: OpenSansBold;
                background-color: #e0e0e0;
                border-radius: 0px;
                margin-right: 10px;
                padding: 5px 15px;
				-webkit-transition: all .25s;
				-moz-transition: all .25s;
				-ms-transition: all .25s;
				-o-transition: all .25s;
				transition: all .25s;
            }
            .custom-paginacao a:hover,
            .custom-paginacao span:hover {
				-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
				-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
				box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
            }
            .custom-paginacao span.current {
				color: white;
				background-color: #666666;
            }
            .custom-paginacao span.dots {
                /*color: #428bca;*/
                color: #373435;
            }
            .custom-paginacao a:first-child,
            .custom-paginacao span:first-child {
                /*border-bottom-left-radius: 5px;*/
                /*border-top-left-radius: 5px;*/
            }
            .custom-paginacao a:last-child,
            .custom-paginacao span:last-child {
                /*border-bottom-right-radius: 5px;*/
                /*border-top-right-radius: 5px;*/
                margin-right: 0px;
            }
            @media (max-width: 768px) {
                .custom-paginacao {
                    display: inline-block;
                }
                .custom-paginacao a,
                .custom-paginacao span {
                    margin-bottom: 10px !important;
                    display: inline-block;
                }
            }
        </style>
    <?php
}


// add_theme_support( 'wc-product-gallery-zoom' );
// add_theme_support( 'wc-product-gallery-lightbox' );
// add_theme_support( 'wc-product-gallery-slider' );

add_shortcode('breadcrumb', function(){
	ob_start();
	?>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php
				if(function_exists('bcn_display')) {
					bcn_display();
				}
			?>
		</div>
	<?php
	return ob_get_clean();
});

/**
 * Criando uma area de widgets
 *
 */
function widgets_novos_widgets_init() {

	register_sidebar( array(
		'name' => 'WooCommerce Sidebar',
		'id' => 'sidebar_woocommerce',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'widgets_novos_widgets_init' );



/**
 * @snippet       Remove SALE badge @ Product Archives and Single Product
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=17429
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.4.5
 */
// remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );




function get_tax_checkbox($tax, $id = '', $exclude = array()) {
	$categorias = get_terms(
		array(
			'taxonomy' => $tax,
			'parent' => (($id !== '') ? $id : 0),
			'hide_empty' => true,
			'exclude' => $exclude
		)
	);
	if ( !is_wp_error( $categorias ) ) {
		echo '<ul class="lista-filtro '.(($id !== '') ? 'nivel-filho' : 'nivel-pai').'">';
			foreach ($categorias as $categoria) {

				if (isset($_GET[$tax])) {
					if (strpos($_GET[$tax], $categoria->slug) !== false) {
						$checked = 'checked="checked"';
					} else {
						$checked = '';
					}
				}

				if ($categoria->parent == 0) {
					$categoria_pai = '';
				} else {
					$categoria_pai = ' ('.$categoria->count.')';
				}

				$subtax = get_term_children($categoria->term_id, $tax);
				echo '<li>';
					echo '<div class="custom-control custom-checkbox">';
						echo '<input type="checkbox" class="custom-control-input check-'.$tax.'" id="custom-check-'.$categoria->slug.'" slug="'.$categoria->slug.'" '.$checked.'>';
						// echo '<label class="custom-control-label" for="custom-check-'.$categoria->slug.'" data-toggle="collapse" data-target="#collapse-'.$categoria->slug.'" aria-expanded="false" aria-controls="collapse-'.$categoria->slug.'">'.$categoria->name.'</label>';
						echo '<label class="custom-control-label" for="custom-check-'.$categoria->slug.'"><button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#collapse-'.$categoria->slug.'" aria-expanded="false" aria-controls="collapse-'.$categoria->slug.'">'.$categoria->name.' '.$categoria_pai.'</button></label>';
					echo '</div>';
					if (is_array($subtax) and (count($subtax) >= 1)) {
						echo '<div class="collapse" id="collapse-'.$categoria->slug.'">';
							get_tax_checkbox($tax, $categoria->term_id);
						echo'</div>';
					}
				echo '</li>';
			}
		echo'</ul>';
	}
}

function get_tax_hierarchical($tax, $id = '', $exclude = array(), $is_sub = false) {
	$categorias = get_terms(
		array(
			'taxonomy' => $tax,
			'parent' => (($id !== '') ? $id : 0),
			'hide_empty' => true,
			'exclude' => $exclude
		)
	);
	if ( !is_wp_error( $categorias ) ) {
		echo '<ul class="'.(($is_sub) ? 'subcategorias' : 'categorias collapse').'" '.(($is_sub) ? '' : 'id="collapseCategorias"').'>';
			foreach ($categorias as $categoria) {
				$subtax = get_term_children($categoria->term_id, $tax);
				echo '<li class="'.(($is_sub) ? 'subcategoria-item' : 'categoria-item').'">';
					echo '<a href="'.get_term_link($categoria->term_id).'">'.$categoria->name.'</a>';
					if (is_array($subtax) and (count($subtax) >= 1)) {
						get_tax_hierarchical($tax, $categoria->term_id, array(), true);
					}
				echo '</li>';
			}
		echo'</ul>';
	}
}

add_shortcode('barra-lateral', function(){
	ob_start();
	?>
	<div class="pagina-loja" slug="<?php echo get_the_permalink(wc_get_page_id('shop')); ?>"></div>
	<div class="filtro-fundo">
		<div class="filtro-titulo">
			<h3 class="texto">FILTRO</h3>
		</div>
		<div class="filtro filtro-categorias">
			<button class="btn btn-primary botao-titulo" type="button" data-toggle="collapse" data-target="#collapseCategoriasFiltro" aria-expanded="<?php echo (isset($_GET['product_cat']) ? 'true' : 'false'); ?>" aria-controls="collapseCategoriasFiltro">Categorias</button>
			<div class="collapse categorias <?php echo (isset($_GET['product_cat']) ? 'show' : ''); ?>" id="collapseCategoriasFiltro">
				<?php get_tax_checkbox('product_cat', '', array(15, 179)); ?>
			</div>
		</div>
		<div class="filtro filtro-marcas">
			<button class="btn btn-primary botao-titulo" type="button" data-toggle="collapse" data-target="#collapseMarcas" aria-expanded="<?php echo (isset($_GET['product_brand']) ? 'true' : 'false'); ?>" aria-controls="collapseMarcas">Marcas</button>
			<div class="collapse categorias <?php echo (isset($_GET['product_brand']) ? 'show' : ''); ?>" id="collapseMarcas">
				<?php get_tax_checkbox('product_brand'); ?>
			</div>
		</div>
	</div>
	<script>
		jQuery(document).ready(function($) {
			var categorias_selecionadas = verificaSelecoes('.check-product_cat');
			var marcas_selecionadas = verificaSelecoes('.check-product_brand');

			verificaSelecoes();

			$('.check-product_cat').on('change', function(event) {
				console.log('ANTES: '+categorias_selecionadas);
				if ($(this).is(':checked')) {
					categorias_selecionadas.push($(this).attr('slug'));
				} else {
					categorias_selecionadas.splice(categorias_selecionadas.indexOf($(this).attr('slug')), 1);
				}
				// console.log(montaParametrosCategorias(categorias_selecionadas));
				montaUrl();
				console.log('DEPOIS: '+categorias_selecionadas);
			});

			$('.check-product_brand').on('change', function(event) {
				if ($(this).is(':checked')) {
					marcas_selecionadas.push($(this).attr('slug'));
				} else {
					marcas_selecionadas.splice(marcas_selecionadas.indexOf($(this).attr('slug')), 1);
				}
				// console.log(montaParametrosCategorias(marcas_selecionadas));
				montaUrl();
			});

			function verificaSelecoes(classe) {
				let aux = [];
				$(classe).each(function(index, el) {
					if ($(this).is(':checked')) {
						// console.log('CHECK: '+$(this).attr('slug'));
						aux.push($(this).attr('slug'));
					}
				});
				// console.log(categorias_selecionadas);
				return aux;
			}

			function montaParametrosCategorias(vetor) {
				// console.log(vetor.length);
				let ret = '';
				$.each(vetor, function(index, elemento){
					ret += ((ret == '') ? '' : ',')+elemento;
				});
				return ret;
			}

			function montaUrl() {

				let parametros_url = '';

				if (categorias_selecionadas.length >= 1) {
					parametros_url += ((parametros_url == '') ? '?' : '&')+'product_cat='+montaParametrosCategorias(categorias_selecionadas);
				}

				if (marcas_selecionadas.length >= 1) {
					parametros_url += ((parametros_url == '') ? '?' : '&')+'product_brand='+montaParametrosCategorias(marcas_selecionadas);
				}

				console.log(parametros_url);

				window.location.replace($('.pagina-loja').attr('slug')+parametros_url+'<?php echo ((isset($_GET['orderby'])) ? '&orderby='.$_GET['orderby'] : ''); ?>');
			}
		});
	</script>
	<?php
	return ob_get_clean();
});



/* Add Checkbox to send mail copy to self */
/*
add_filter( 'wpcf7_additional_mail', 'wpcf7_send_mail_to_self', 10, 2 );
 
function wpcf7_send_mail_to_self( $additional_mail, $cf ) {
 if ( "Enviar uma cópia para meu cliente" != $cf->posted_data['enviar_copia_cliente'][0] )
 $additional_mail = array();
 
 return $additional_mail;
}
*/

add_filter( 'get_product_search_form' , 'me_custom_product_searchform' );
function me_custom_product_searchform( $form ) {
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '" class="form-inline formBusca">
		
			<label class="screen-reader-text" for="s">' . __( 'Search for:', 'woocommerce' ) . '</label>
			<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Buscar produtos, marcas e muito mais..." class="form-control txtBusca" />
			<button type="submit" id="searchsubmit" class="btn btn-default btnBuscar" />
				<img src="'.TEMA_URL.'/svg/lupa-busca.svg'.'" alt="Buscar">
			</button>
			<input type="hidden" name="post_type" value="product" />
		
	</form>';
	return $form;
}

add_shortcode('pagina-indicacoes', function(){
	ob_start();
	require_once('pagina-indicacoes.php');
	return ob_get_clean();
});


add_action('wp_head', function(){
	echo '
		<script>
			var timeout;
			jQuery( function( $ ) {
			    $(\'.woocommerce\').on(\'change\', \'input.qty\', function(){
			 
			        if ( timeout !== undefined ) {
			            clearTimeout( timeout );
			        }
			 
			        timeout = setTimeout(function() {
			            $("[name=\'update_cart\']").trigger("click");
			        }, 1000 ); // 1 segundo de atraso, meio segundo (500) também parece bacana de deixar
			 
			    });
		    } );
		</script>
		<style>
			.woocommerce button[name="update_cart"],
			.woocommerce input[name="update_cart"] {
				display: none;
			}
		</style>
	';
});


function produtos_carrinho() {
	?>
	<div class="produtos-carrinho">
		<?php
			$carrinho_produtos = WC()->cart->get_cart();

			if (WC()->cart->get_cart_contents_count() >= 1) {
				foreach ($carrinho_produtos as $carrinho_produto_key => $carrinho_produto) {
					$_carrinho_produto = apply_filters( 'woocommerce_cart_item_product', $carrinho_produto['data'], $carrinho_produto, $carrinho_produto_key );
					$carrinho_produto_nome = $carrinho_produto['data']->get_title();
					$carrinho_produto_quantidade = $carrinho_produto['quantity'];
					$carrinho_produto_preco = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_carrinho_produto ), $carrinho_produto, $carrinho_produto_key );
					$carrinho_produto_imagem = apply_filters( 'woocommerce_cart_item_thumbnail', $_carrinho_produto->get_image(), $carrinho_produto, $carrinho_produto_key );
					$carrinho_produto_id = apply_filters( 'woocommerce_cart_item_product_id', $carrinho_produto['product_id'], $carrinho_produto, $carrinho_produto_key );
					$carrinho_produto_remover = apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
													'woocommerce_cart_item_remove_link',
													sprintf(
														'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
														esc_url( wc_get_cart_remove_url( $carrinho_produto_key ) ),
														esc_html__( 'Remove this item', 'woocommerce' ),
														esc_attr( $carrinho_produto_id ),
														esc_attr( $_carrinho_produto->get_sku() )
													),
													$carrinho_produto_key
												);
					?>
					<div class="produto-carrinho">
						<div class="produto-carrinho-interno">
							<div class="remover"><?php echo $carrinho_produto_remover; ?></div>
							<div class="informacoes">
								<div class="imagem">
									<a href="<?php echo get_the_permalink($carrinho_produto_id); ?>" alt="<?php echo $carrinho_produto_nome; ?>" title="<?php echo $carrinho_produto_nome; ?>"><?php echo $carrinho_produto_imagem; ?></a>
								</div>
								<div class="dados">
									<div class="nome">
										<a href="<?php echo get_the_permalink($carrinho_produto_id); ?>" alt="<?php echo $carrinho_produto_nome; ?>" title="<?php echo $carrinho_produto_nome; ?>"><?php echo $carrinho_produto_nome; ?></a>
									</div>
									<div class="valores">
										<span class="quantidade">Quantidade: <?php echo $carrinho_produto_quantidade; ?></span>
										<span class="preco">Valor: <?php echo $carrinho_produto_preco; ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				<div class="text-center opcoes-carrinho">
					<a href="<?php echo wc_get_cart_url(); ?>" class="ir-ao-carrinho">Ir para meu carrinho</a>
				</div>
				<?php
			} else {
				echo 'Nenhum produto no carrinho.';
			}
		?>
	</div>
	<?php
}


add_filter( 'woocommerce_add_to_cart_fragments', 'wc_refresh_mini_cart_count');
function wc_refresh_mini_cart_count($fragments){
    ob_start();
    ?>
    <span class="qtdItensCarrinho"><?php echo WC()->cart->get_cart_contents_count(); ?>&nbsp;<?php echo ((WC()->cart->get_cart_contents_count() == 1) ? 'produto' : 'produtos'); ?></span>
    <?php
        $fragments['.qtdItensCarrinho'] = ob_get_clean();
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'wc_refresh_mini_cart_count_produtos');
function wc_refresh_mini_cart_count_produtos($fragments){
    ob_start();
	produtos_carrinho();
    $fragments['.produtos-carrinho'] = ob_get_clean();
    return $fragments;
}




add_shortcode('sidebar-noticias', function(){
	ob_start();
	?>
	<div class="barra-lateral-noticias">

		<div class="bloco-barra-lateral">
			<div class="titulo">
				<h4 class="texto">CONHEÇA NOSSOS PRODUTOS</h4>
			</div>
			<div class="descricao">
				<div class="slider-produtos">
					<?php echo do_shortcode('[featured_products per_page="12" columns="1"]'); ?>
				</div>
				<div class="botao-ir-pra-loja">
					<a href="<?php echo get_permalink(woocommerce_get_page_id('shop' )); ?>">IR PARA LOJA</a>
				</div>
			</div>
			<script>
				$('.slider-produtos ul.products').slick({
					infinite: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: false
				});
			</script>
		</div>

		<div class="bloco-barra-lateral">
			<div class="titulo">
				<h4 class="texto">CATEGORIAS</h4>
			</div>
			<div class="descricao">
				<div class="categorias">
					<?php
						$categorias = get_terms(
							array(
								'taxonomy' => 'tax_noticias_categoria',
							)
						);
						foreach ($categorias as $categoria) {
							?>
							<div class="categoria">
								<p class="texto">
									<a href="<?php echo get_term_link($categoria->term_id, 'tax_noticias_categoria'); ?>"><?php echo $categoria->name; ?></a>
								</p>
							</div>
							<?php
						}
					?>
				</div>
			</div>
		</div>

		<div class="bloco-barra-lateral">
			<div class="titulo">
				<h4 class="texto">NOTÍCIAS MAIS VISTAS</h4>
			</div>
			<div class="descricao">
				<div class="noticias-mais-vistas">
					<?php
						$args = array(
							'post_html' => '<li class="bloco-noticia-mais-vista">
								<div class="imagem">
									<a href="{url}">
										<img src="{thumb_url}" alt="{text_title}" title="{text_title}">
									</a>
								</div>
								<div class="informacoes">
									<div class="titulo">
										<a href="{url}" alt="{text_title}" title="{text_title}">{text_title}</a>
									</div>
									<div class="sumario">
										<p>'.strip_tags('{summary}').'</p>
									</div>
									<div class="continuar-lendo">
										<a href="{url}">CONTINUAR LENDO</a>
									</div>
								</div>
							</li>',
							'post_type' => 'cpt_noticias',
							'limit' => 3,
							'thumbnail_width' => 80,
							'thumbnail_height' => 100,
							'excerpt_length' => 80,
						);
						wpp_get_mostpopular( $args );
					?>
				</div>
			</div>
		</div>

	</div>
	<?php
	return ob_get_clean();
});





/* ============================================================================================================================== */
/* ============================================================================================================================== */
/* ============================================ ADICIONAR ITEM/PAGINA NO MINHA-CONTA ============================================ */
/* ============================================================================================================================== */
/* ============================================================================================================================== */

// ===============================================================
// ============================== 1 ==============================
// ===============================================================
function iconic_account_menu_items( $items ) {
    // $items['indicacoes'] = __( 'Indicações', 'iconic' );
    // return $items;

	$items = array_slice($items, 0, 5, true) +
			 array('indicacoes' => 'Indicações') +
			 // array('teste' => 'Teste') +
			 array_slice($items, 5, NULL, true);
	return $items;
}
add_filter( 'woocommerce_account_menu_items', 'iconic_account_menu_items', 10, 1 );

// ===============================================================
// ============================== 2 ==============================
// ===============================================================
function iconic_add_my_account_endpoint() {
    add_rewrite_endpoint( 'indicacoes', EP_PAGES );
    // add_rewrite_endpoint( 'teste', EP_PAGES );
}
add_action( 'init', 'iconic_add_my_account_endpoint' );

// ===============================================================
// ============================== 3 ==============================
// ===============================================================
function iconic_information_endpoint_content() {
    echo do_shortcode('[pagina-indicacoes]');
}
add_action( 'woocommerce_account_indicacoes_endpoint', 'iconic_information_endpoint_content' );

// function iconic_teste_endpoint_content() {
//     echo 'TESTE';
// }
// add_action( 'woocommerce_account_teste_endpoint', 'iconic_teste_endpoint_content' );







/**
 * @snippet       Merge Two "My Account" Tabs @ WooCommerce Account
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @source        https://businessbloomer.com/?p=73601
 * @author        Rodolfo Melogli
 * @compatible    Woo 3.5.3
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
// -------------------------------
// 1. First, hide the tab that needs to be merged/moved (edit-address in this case)
 
add_filter( 'woocommerce_account_menu_items', 'bbloomer_remove_address_my_account', 999 );
  
function bbloomer_remove_address_my_account( $items ) {
unset($items['edit-address']);
return $items;
}
 
// -------------------------------
// 2. Second, print the ex tab content into an existing tab (edit-account in this case)
 
add_action( 'woocommerce_account_edit-account_endpoint', 'woocommerce_account_edit_address' );

add_action( 'woocommerce_account_dashboard_endpoint', 'woocommerce_account_orders' );








/**
 * @snippet       Add First & Last Name to My Account Register Form - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21974
 * @author        Rodolfo Melogli
 * @credits       Claudio SM Web
 * @compatible    WC 3.5.2
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
///////////////////////////////
// 1. ADD FIELDS
 
add_action( 'woocommerce_register_form_start', 'bbloomer_add_name_woo_account_registration' );
 
function bbloomer_add_name_woo_account_registration() {
    ?>
 
    <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>
 
    <p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>
 
    <div class="clear"></div>
 
    <?php
}
 
///////////////////////////////
// 2. VALIDATE FIELDS
 
add_filter( 'woocommerce_registration_errors', 'bbloomer_validate_name_fields', 10, 3 );
 
function bbloomer_validate_name_fields( $errors, $username, $email ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
    }
    return $errors;
}
 
///////////////////////////////
// 3. SAVE FIELDS
 
add_action( 'woocommerce_created_customer', 'bbloomer_save_name_fields' );
 
function bbloomer_save_name_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        update_user_meta( $customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']) );
    }
    if ( isset( $_POST['billing_last_name'] ) ) {
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        update_user_meta( $customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']) );
    }
 
}