<form action="<?php echo home_url(); ?>" method="get" class="form-inline formBusca">
	<input type="text" name="s" value="<?php the_search_query(); ?>" class="form-control txtBusca" placeholder="Buscar produtos, marcas e muito mais...">
	<button type="submit" class="btn btn-default btnBuscar">
		<img src="<?php echo TEMA_URL.'/svg/lupa-busca.svg' ?>" alt="Buscar">
	</button>
</form>